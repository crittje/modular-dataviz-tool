function createNetworkGraph(data, xInput, yInput) {
    console.log(data, xInput, yInput);
    const MARGIN = { top: 20, right: 20, bottom: 30, left: 40, 'legend': 30 };
    const WIDTH = 960;
    const HEIGHT = 500;
    const STRENGTH = -100;
    const NODES = data[0].nodes;
    const LINKS = data[0].links;
    const PIE = data[0].pie;

    var pieData = new Array(data[0].links);
    pieData = data[0].links;

    var distance = 300;
    var link;
    var node;
    var uniqueKeys = getUniqueKeys(PIE);
    var colorMap = getColorMap();
    console.log(PIE);


    var svg = d3.select('#visuals').append('svg')
        .attr('width', WIDTH)
        .attr('height', HEIGHT);

    var simulation = d3.forceSimulation();
    var force = simulation
        .force('center', d3.forceCenter(WIDTH / 2, HEIGHT /2))
        .force('charge', d3.forceManyBody().strength(STRENGTH))
        .force('link', d3.forceLink())
    
    force.nodes(NODES)
    forceLinks = force.force('link')
        .links(LINKS)
        .distance(distance)

    createLinks();
    createNodes();
    addTick();

    function createLinks() {
        link = svg.selectAll('.link')
            .data(LINKS)
            .enter().append('line')
            .attr('class', 'link')
    }

    function createNodes() {
        node = svg.selectAll('.node')
            .data(NODES)
          .enter().append('g')
            .attr('class', 'node')

        var nodeTypes = node.append(function(d) {
                if (d.group === yInput) {
                    return document.createElementNS('http://www.w3.org/2000/svg','g')
                } else {
                    return document.createElementNS('http://www.w3.org/2000/svg', 'rect');
                }
            })
            .attr('id', function(d) {
                if (d.group === yInput) {
                    return 'pie_' + d[xInput].toLowerCase();
                } else {
                    return 'rect_' + d[xInput].toLowerCase();
                }
            })
            .attr('class', function(d) {
                if (d.group === yInput) {
                    return 'arc-node';
                } else {
                    return 'rectangle-node';
                }
            }).attr('fill', 'red');

            createRectangleNodes();
            setTimeout(createCirlceNodes(), 100);

    }

    function addTick() {
        force.on('tick', function() {
            link.attr('x1', function (d) { return d.source.x  })
            link.attr('y1', function (d) { return d.source.y  })
            link.attr('x2', function (d) { return d.target.x  })
            link.attr('y2', function (d) { return d.target.y  })

            node.attr('transform', function (d) { return 'translate(' + d.x + ',' + d.y + ')'; });
        })
    }

    function createCirlceNodes() {
        var arc = d3.arc()
            .outerRadius(20)
            .innerRadius(0);

        var pie = d3.pie()
            .sort(null)
            .value(function(d) { return d.Amount; });

        uniqueKeys.forEach(function(key) {
            var pieGroup = svg.select('#pie_' + key).append('g')
                .attr('class', 'arc').selectAll('g')
                .data(pie(PIE.filter(function(d) { return d[xInput].toLowerCase() == key; })))
              .enter().append('path')
                .attr('d', arc)
                .attr('fill', function(d, i) { 
                    var name = d.data.Retweeted.toLowerCase();
                    return colorMap[name];
                })
        })
    }

    function getColorMap() {
        var colorMap = {};
        for (var i in uniqueKeys) {
            var name = uniqueKeys[i];
            colorMap[name] = d3.schemeCategory20[i];
        }
        return colorMap;
    }

    function createRectangleNodes() {
        svg.selectAll('.rectangle-node')
            .attr('height', '15px')
            .attr('width', '15px')
            .attr('fill', function(d, i) { 
                var name = d[xInput].toLowerCase();
                return colorMap[name]; })
    }

    function getUniqueKeys(dataWithkeys) {
        var results = [];
        var lookUp = {};
        for (var i in dataWithkeys) {
            var key = dataWithkeys[i][xInput].toLowerCase();
            var keyTwo = dataWithkeys[i].Retweeted.toLowerCase();
            addToUniqueKeys(key, lookUp, results)
            addToUniqueKeys(keyTwo, lookUp, results);

        }
        return results;
    }

    function addToUniqueKeys(key, lookUp, results) {
        if (!(key in lookUp)) {
            lookUp[key] = 1;
            results.push(key)
        }
    }
}