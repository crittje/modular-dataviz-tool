var menuData;
var graphs;
var xInput;
var yInput;
var zInput;
var visualData;
var selectedGraph;

$(document).ready(function() {
    getData();

    $('#graph-select').on('change', function() {
        xInput = yInput = zInput = null;
        selectedGraph = $(this).val();
        $('.menu-element').remove();
        $('.menu-element').remove();
        $('#z-select li').remove();
        getSelectedObject(selectedGraph);
        $('select').material_select();
    })

    $(this).on('change', '#x-select, #y-select, #z-select', function() {
        var selectTagId = $(this).attr('id');
        if (selectTagId === 'x-select') {
            xInput = $(this).val();
        } else if (selectTagId === 'y-select') {
            yInput = $(this).val();
        } else {
            zInput = $(this).val();
        }
    })

    $('#generate-btn').on('click', function() {
        $('#visuals').empty();
        getVisualData();
    })
});

function getVisualData() {
    $.getJSON('/data/data.json', function(data) {
        if (selectedGraph === 'barchart')  {
            createBarChart(data.xy, xInput, yInput);
        } else if (selectedGraph === 'donutchart') {
            createDonut(data.xy, xInput, yInput)
        } else if (selectedGraph === 'linechart') {
            createLineChart(data.xyz, xInput, yInput, zInput);
        } else if (selectedGraph === 'Stacked Bar Chart') {
            createStackedBarChart(data.xyz, xInput, yInput, zInput);
        } else if (selectedGraph === 'Network') {
            createNetworkGraph(data.network, xInput,yInput)
        }
    })
    .fail(function(d, textStatus,error) {
        console.error("getJSON failed, status: " + textStatus + ", error: "+error)
    });
}

function getSelectedObject(selected) {
    for (var i = 0; i < graphs.length; i++) {
        if (graphs[i].name == selected) {
            var selectedObj = graphs[i];
            var graphLevels = selectedObj.levels;
            if (graphLevels === 'xy' || graphLevels == 'network') {
                enableLevelDropdowns(selectedObj, ['x', 'y']);
            } else {
                enableLevelDropdowns(selectedObj, ['x', 'y', 'z']);
            }
        }
    }
}

function enableLevelDropdowns(selectedObj, levelArray) {
    for (var i in levelArray) {
        var lvl = levelArray[i];
        var selectTag = $('#' + lvl + '-select').prop('disabled', false);
        if (lvl === 'x' || lvl === 'y') {
            $('#' + lvl + '-label').text('Select ' + lvl + ' variable')
            addElementsToDropdown(lvl, selectedObj[lvl]);
            if (lvl === 'y') {
                $('#z-label').text('Not required');
                $('#z-select').prop('disabled', true);
            }
        } else {
            addElementsToDropdown(lvl, selectedObj[lvl]);
            $('#' + lvl + '-label').text('Per:')
        }

    }
}

function getData() {
    $.getJSON('/data/menu.json', function(data) {
        graphs = data.graphs
        addGraphsToMenu(graphs);
        $('select').material_select();
    })
    .fail(function(d, textStatus,error) {
        console.error("getJSON failed, status: " + textStatus + ", error: " + error);
    });
}

function addGraphsToMenu(graphs) {
    var graphDropdown = $('#graph-select');
    for (var i = 0; i < graphs.length; i++) {
        var graph = graphs[i];
        graphDropdown.append('<option value=\"' + graph.name + '\">' + graph.name + '</option>');
    }
}

function addElementsToDropdown(tagId, elements) {
    var dropdown = $('#' + tagId + '-select');
    for (var i in elements) {
        dropdown.append('<option class="menu-element" value=\"' + elements[i] + '\">' + elements[i] + '</option>');
    }
}

