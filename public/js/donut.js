const COLOR = d3.schemeCategory20;
var total;
var radius;
var pie;
var dataLength;

function createDonut(data, xInput, yInput) {
    const MARGIN = { "bottom" : 20, "left": 40, "top" : 50, "right" : 30};
    const SIZES = { 'width' : 600 - MARGIN.left, 'height': 600};
    radius = Math.min(SIZES.width, SIZES.height) / 2 - 50;
    total = d3.sum(data.map(function(d) { return d[yInput]; }));
    dataLength = data.length;

    var tooltipGroup = d3.select('#visuals').append('g')
            .attr('class', 'tooltip');

    var svg = d3.select('#visuals').append('svg')
            .attr('width', SIZES.width + MARGIN.left + MARGIN.right)
            .attr('height', SIZES.height)
          .append('g')
            .attr('transform', 'translate(' + ((SIZES.width / 2) + ',' + SIZES.height / 2 + ')'))

    svg.append('g')
        .attr('class', 'donut-label-name');


    var arc = radiusProperties(radius);
    var labelArc = radiusProperties(radius + 30);
    pie = donutAnglesProperties();

    addPies(data, svg, arc, tooltipGroup);
    addLines(labelArc, arc, data, svg);
    createLegend(data, svg, SIZES, MARGIN);
    addCenterText(svg);
    addLabels(data, svg, labelArc, yInput);
}

function addPies(data, svg, arc, tooltipGroup) {
    var g = svg.selectAll('path')
            .data(pie(data))
          .enter().append('g')
            .attr('id', function(d, i) { return 'piece_' + i })
            .attr('class', 'arc');
            
    g.append('path')
        .attr('d', arc)
        .attr('fill', function(d, i) { return COLOR[i % 20]});
    handlePieMouseEvents(tooltipGroup, g);
}

function radiusProperties(r) {
    return d3.arc()
            .innerRadius(r- 75)
            .outerRadius(r);
}

function donutAnglesProperties() {
    return d3.pie()
            .value(function (d) { return d[yInput] })
            .sort(null)
            .padAngle(.03);
}

function createLegend(data, svg, SIZES, MARGIN) {
    const LEGENDSPACING = 4;
    const LEGENDSIZE = 18;

    var legend = svg.selectAll('.legend')
        .data(data)
      .enter().append('g')
        .attr('class', 'legend')
        .attr('transform', function(d, i) {
            var legendHeight = LEGENDSIZE + LEGENDSPACING;
            var horizontalPosition = -2 * legendHeight + SIZES.width / 2;
            var verticalPosition = ((i * 25) - SIZES.height / 2) + MARGIN.top ;
            return 'translate(' + horizontalPosition + ',' + verticalPosition + ')';
        })

        legend.append('rect')
            .attr('width', LEGENDSIZE)
            .attr('height', LEGENDSIZE)
            .attr('stroke-width', 2)
            .style('fill', function(d, i) { return COLOR[i % 20] })
            .style('stroke', COLOR)

        legend.append('text')
            .attr('class', 'left-align')
            .attr('x', LEGENDSIZE + LEGENDSPACING + 30)
            .attr('y', LEGENDSIZE - LEGENDSPACING)
            .text(function(d) { return d[xInput]})

        handleLegendMouseEvents(legend);
}


function handleLegendMouseEvents(legend) {
    legend.on('mouseover', function(d, i) {
        for (var j = 0; j < dataLength; j++) {
            if (j == i) {
                continue;
            } 
            d3.select('#piece_' + j).transition()
                .duration(100)
                .style('opacity', 0.5);
        }
    })
    .on('mouseout', function() {
        for (var i = 0; i < dataLength; i++) {
            d3.select('#piece_' + i).transition()
                .duration(100)
                .style('opacity', 1);
        }
    })
}

function handlePieMouseEvents(tooltipGroup, g) {

    g.on('mouseover', function(d, i) {
        d3.select('#piece_' + i)
            .style('opacity', .7);
        tooltipGroup.transition()
            .duration(100)
            .style('opacity', .9)
            .style('background', function() { return COLOR[i % 20]})
            tooltipGroup.html('<b>' + xInput + ':</b> ' + d.data[xInput] + 
                '<br><b>' + yInput + '</b>: ' + d.data[yInput] +
                '<br><b>Percentage:</b> ' + calculatePercentage(d.data[yInput], total))
    })
    g.on('mousemove', function() {
        tooltipGroup.style('left', (d3.event.pageX + 15) + 'px')
            .style('top', d3.event.pageY + 'px');
    })
    g.on('mouseout', function(d, i) {
        d3.select('#piece_' + i).style('opacity', 1);
        tooltipGroup.transition()
            .duration(150)
            .style('opacity', 0);
    })
}


function addCenterText(svg) {
    svg.append('text')
        .attr('text-anchor', 'middle')
        .attr('dy', '0em')
        .style('opacity', .8)
        .html('Total amount of ' + yInput + ': ' + total);
    
    svg.append('text')
        .attr('text-anchor', 'middle')
        .attr('dy', '1em')
        .style('opacity', .6)
        .html('Hover over pies for more details!')
}

function addLabels(data, svg, labelArc, yInput) {

    var label = svg.select('.donut-label-name').selectAll('text')
        .data(pie(data))
      .enter()
        .append('text')
        .attr('dy', '.35em')
        .attr('transform', function (d) {
            var labelRadius = radius + 30;
            var pos = labelArc.centroid(d);
            // pos[0] = radius * 0.95 * (midAngle(d) < Math.PI ? 1 : -1);
            x = pos[0];
            y = pos[1];
            h = Math.sqrt(x*x + y*y);
            return "translate(" + (x/h * labelRadius) +  ',' + (y/h * labelRadius) +  ")"; 
            // return "translate(" + pos + ")"; 
        })
        .html(function(d) { return calculatePercentage(d.data[yInput]) })
        .style('text-anchor', function(d) {
            // if slice centre is on the left, anchor text to start, otherwise anchor to end
            return (midAngle(d)) < Math.PI ? 'start' : 'end';
        });
}

function addLines(labelArc, innerArc, data, svg) {

    svg.append('g')
        .attr('class', 'polylines');

    var polyline = svg.select('.polylines').selectAll('polyline')
        .data(pie(data))
      .enter()
        .append('polyline')
        .attr('points', function(d) {
            var pos = labelArc.centroid(d);
            x = pos[0];
            y = pos[1];
            h = Math.sqrt(x*x + y*y);
            return [labelArc.centroid(d), labelArc.centroid(d), [x/h * (radius + 20), y/h * (radius + 20)]]
        })
}

function calculatePercentage(value) {
    var piece = value / total;
    return Math.round(piece * 100) + '%'
}

function midAngle(d) { return d.startAngle + (d.endAngle - d.startAngle) / 2; }