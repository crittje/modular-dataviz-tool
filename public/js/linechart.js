function createLineChart(data, xInput, yInput) {

    const MARGIN = { 'bottom': 40, 'top': 20, 'left': 40, 'right': 40, 'label' : 150}
    const SIZES = { 'width' : 960 - MARGIN.left - MARGIN.right, 'height' : 550 - MARGIN.top - MARGIN.bottom }

    var x = d3.scaleLinear()
            .range([0, SIZES.width]);
    var y = d3.scaleLinear()
            .rangeRound([0, SIZES.height]);
    var z = d3.scaleOrdinal(d3.schemeCategory20);
        
    var yMax = d3.max(data, function(d) { return d[yInput] })
    var xMax = getLastDayDynamically(data);
    var ceiledMax = ceilByFive(yMax);

    x.domain([1, xMax]).nice();
    y.domain([ceiledMax, 0]);

    var toolTipDiv = d3.select('#visuals').append('g')
        .attr('class', 'tooltip')
        .style('opacity', 0);

    var svg = d3.select('#visuals').append('svg')
        .attr('width', SIZES.width + MARGIN.left + MARGIN.right + MARGIN.label)
        .attr('height', SIZES.height + MARGIN.top + MARGIN.bottom)
      .append('g')
        .attr('transform', 'translate(' + MARGIN.left + ',' + MARGIN.top + ')');

    var line = d3.line()
            .x(function(d) { return x(d[zInput]) })
            .y(function(d) { return y(d[yInput]) })
    
    var nestedData = d3.nest()
        .key(function(d) { return d[xInput] })
        .entries(data);

    creatGridLines();
    createAxis();
    createLines();
    addLegend();
    addLabels();
    addObservationDots();
    
    function ceilByFive(max) {
        return Math.ceil(max / 5) * 5
    }    
    
    function getLastDayDynamically() {
        var lastObservation = 1;
        for (var i in data) {
            if (data[i][zInput] > lastObservation) {
                lastObservation = data[i][zInput];
            }
        }
        return lastObservation;
    }

    function creatGridLines() {
        svg.append('g')			
            .attr('class', 'grid')
            .call(make_y_gridlines(y, yMax));
        
        svg.append('g')
            .attr('class', 'grid')
            .attr('transform', 'translate(0,' + SIZES.height + ')')
            .call(d3.axisBottom(x).ticks(xMax).tickSize(-SIZES.height).tickFormat(''));
    }

    function make_y_gridlines(y, max) {		
        return d3.axisLeft(y)
            .ticks(5)
            .tickSize(-SIZES.width)
            .tickFormat('')
    }

    function createAxis() {
        svg.append('g')
            .attr('transform', 'translate(0,' + SIZES.height + ')')
            .call(d3.axisBottom(x).ticks(xMax));
    
        svg.append('g')
            .call(d3.axisLeft(y).ticks(5))
    }

    function createLines() {
        nestedData.forEach(function(d) {
            svg.append('path')
                .attr('id', 'line_' + d.key)
                .attr('class', 'line')
                .style('stroke', function() { return z(d.key) })
                .attr('d', line(d.values));
            }
        );
    }

    function addObservationDots() {
        nestedData.forEach(function(d) {
            for (var observation in d.values) {
                d3.select('svg').select('g').append('circle')
                    .attr('class', 'x-value-' + observation + ' dot-' + d.key)
                    .attr('r', 5)
                    .attr('cx', function() { return x(d.values[observation][zInput]) })
                    .attr('cy', function() { return y(d.values[observation][yInput]) })
                    .style('fill', function() { return z(d.key) })
                    .on('mouseover', function() {
                        var classValue = this.getAttribute('class');
                        var xValue = parseInt(classValue.split('-')[2]);

                        toolTipDiv.transition()
                            .duration(200)
                            .style('opacity', .9) 
                            .style('background', function() { return z(d.key) })
                        toolTipDiv.html(xInput + ': ' + d.key + '<br>' + 
                                        yInput + ': ' + d.values[xValue][yInput])
                            .style('left', d3.event.clientX + 10 + 'px')
                            .style('top', d3.event.clientY + 'px');
                    })
                    .on('mouseout', removeToolTip);
            }
        })
    }

    function removeToolTip() {
        toolTipDiv.transition()
            .duration(750)
            .style('opacity', 0)
    }

    function addLegend() {
        nestedData.forEach(function(d, i) {
            var legend = svg.append('g')
                .attr('class', 'legend')
                .attr('transform', function() {
                    var horizontal = SIZES.width + MARGIN.left - 10;
                    var vertical = (i * 25) + 10;
                    return 'translate(' + horizontal + ',' + vertical + ')';
                })

            legend.append('rect')
                .attr('width', 18)
                .attr('height', 18)
                .attr('class', 'legend-rectangle')
                .style('fill', function() { return z(d.key) })
                .style('stroke', d3.schemeCategory20)
                .on('click', function() {
                    var clickedLegend = d.key;
                    var relatedLineElement = $('#line_' + clickedLegend)
                    var relatedCircleElement = $('.dot-' + clickedLegend)
            
                    if (relatedLineElement.css('display') !== 'none') {
                        relatedLineElement.hide();
                        relatedCircleElement.hide();
                    } else {
                        relatedLineElement.show();
                        relatedCircleElement.show();
                    }
                })

            legend.append('text')
                .attr('x', 60)
                .attr('y', 14)
                .text(d.key);
        })
    }

    function addLabels() {
        svg.append('g').append('text')
            .attr('class', 'text')
            .attr('transform', 'translate(' + (SIZES.width + MARGIN.left)/2 + ',' 
                    + (SIZES.height + 30)+ ')')
            .text(zInput);

        svg.append('g').append('text')
            .attr('class', 'text')
            .attr('transform', 'rotate(-90)')
            .attr('y', - MARGIN.left)
            .attr('x', - (SIZES.height / 2))
            .attr('dy', '1em')
            .text(yInput);
    }
}

