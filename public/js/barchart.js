function createBarChart(data, xInput, yInput) {
    const MARGIN = { "bottom" : 20, "left": 40 };
    const WIDTH = 600 - MARGIN.left;
    const HEIGHT = 600;
    const DATA_LENGTH = data.length;
    var bar;

    var tooltipGroup = d3.select('#visuals').append('g')
        .attr('class', 'tooltip');

    var x = d3.scaleBand()
            .range([0, WIDTH])
            .padding(0.1);
    var y = d3.scaleLinear()
            .range([HEIGHT, 0]);

    x.domain(data.map(function(d) { return d[xInput]}));
    y.domain([0, d3.max(data, function(d) { return d[yInput] + 1; })]);

    var svg = d3.select('#visuals').append('svg')
                .attr('width', WIDTH + MARGIN.left)
                .attr('height', HEIGHT + MARGIN.bottom)
              .append('g')
                .attr('transform', 'translate(' + MARGIN.left + ',0)');

    createBars();
    addAxis();
    addAxis();

    function createBars() {
        var rect = svg.selectAll('.bar')
            .data(data)
          .enter().append('rect')
            .attr('id', function(d, i) { return 'bar_' + i })
            .attr('class', 'bar');

        rect.attr('x', function(d) { return x(d[xInput]); })
            .attr('width', x.bandwidth())
            .attr('y', function(d) { return y(d[yInput]); })
            .attr('height', function(d) { return HEIGHT - y(d[yInput]); });
        handleMouseEvents(rect);
    }

    function addAxis() {
        svg.append('g')
            .attr('transform', 'translate(0,' + HEIGHT +')')
            .call(d3.axisBottom(x));
        
        svg.append('g')
            .call(d3.axisLeft(y));

        svg.append('g').append('text')
            .attr('transform', 'rotate(-90)')
            .attr('x', 0 - ( HEIGHT / 2))
            .attr('y', 0 - MARGIN.left)
            .attr('dy', '1em')
            .text(yInput);
    }

    function handleMouseEvents(rect) {
        rect.on('mouseover', function(d, i) {
            for (var j = 0; j < DATA_LENGTH; j++) {
                if (j == i) {
                    continue;
                }
                d3.select('#bar_' + j)
                    .transition()
                    .duration(100)
                    .style('opacity', .7);
            }
            addTooltip(tooltipGroup, xInput, d[xInput], d[yInput], 'steelblue');
        })
        .on('mousemove', function() { followMouse(tooltipGroup) })
        .on('mouseout', function(d, i) {
            for (var i = 0; i < DATA_LENGTH; i++) {
                d3.select('#bar_' + i)
                    .transition()
                    .duration(100)
                    .style('opacity', 1);
            }
            removeTooltip(tooltipGroup);
        })
    }
}
