function ceilByFive(max) {
    return Math.ceil(max / 5) * 5
} 

function createArrayOfUniqueValues(array, data, key) {

    $.each(data, function(i, value) {
        if ($.inArray(value[key], array) == -1) {
            array.push(value[key]);
        }
    })
}

function addTooltip(toolTipGroup, firstKey, xValue, yValue, color) {
    toolTipGroup.transition()
        .duration(100)
        .style('opacity', 1)
        .style('background', color)
        toolTipGroup.html('<b>' + firstKey + ':</b>' + xValue +
            '<br><b>' + yInput + ':</b> ' + yValue);
}

function removeTooltip(toolTipGroup) {
    toolTipGroup.transition()
        .duration(100)
        .style('opacity', 0);
}

function followMouse(toolTipGroup) {
    toolTipGroup.style('left', (d3.event.pageX + 15) + 'px')
        .style('top', d3.event.pageY + 'px');
}