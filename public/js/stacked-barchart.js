function createStackedBarChart(data, xInput, yInput, zInput) {
    console.log('creating stacked', data, xInput, yInput, zInput)

    const MARGIN = { top: 20, right: 20, bottom: 50, left: 40, label: 30, legend : 150 };
    const WIDTH = 600 - MARGIN.left;
    const HEIGHT = 600;
    const DATA_LENGTH = data.length;   
    
    var dataAsArray = [];
    var stacksPerBar = [];

    var tooltipGroup = d3.select('#visuals').append('g')
        .attr('class', 'tooltip');

    var svg = d3.select('#visuals').append('svg');

    var main = svg.attr('width', WIDTH + MARGIN.left)
        .attr('height', HEIGHT + MARGIN.bottom)
      .append('g')
        .attr('transform', 'translate(' + (MARGIN.right + MARGIN.label) + ',' + MARGIN.top + ')');

    var x = d3.scaleBand()
        .rangeRound([0, WIDTH - MARGIN.legend])
        .padding(0.3)
        .align(0.3);

    var y = d3.scaleLinear()
        .rangeRound([HEIGHT, 0]);

    var z = d3.scaleOrdinal(d3.schemeCategory20);

    var stack = d3.stack();

    transformDataForStack();

    var yMax= d3.max(dataAsArray, function (d) { return d.total });
    var ceiledMax = ceilByFive(yMax);
    var amountOfBars = dataAsArray.length;

    x.domain(data.map(function(d) { return d[xInput] }));
    y.domain([0, ceiledMax]).nice();
    z.domain(dataAsArray);

    createBars();
    createAxis();
    createLabels();
    createLegend();
    createToolTip();

    function createToolTip(rectangle) {
        rectangle.on('mouseover', function(d, i) {
            var element = $(this);
            var xValue = element.attr(zInput);

            var yValue = d[1] - d[0];
            var color = element.attr('fill');
            addTooltip(tooltipGroup, zInput, xValue, yValue, color)
        })
        .on('mousemove', function() { followMouse(tooltipGroup) })
        .on('mouseout', function() { removeTooltip(tooltipGroup) });
    }

    function createLegend() {
        var legend = main.selectAll('.legend')
            .data(stacksPerBar.reverse())
          .enter().append('g')
            .attr('class', 'legend')
            .attr('transform', function(d, i) {
                var vertical = i * 20;
                return 'translate(-' + MARGIN.legend + ',' + vertical + ')';
            })
        legend.append('rect')
            .attr('x', WIDTH + 18)
            .attr('width', 18)
            .attr('height', 18)
            .attr('fill', function(d, i) { return z((dataAsArray.length- 1) - i) });

        legend.append('text')
            .attr('class', 'text')
            .attr('x', WIDTH + 60)
            .attr('y', 9)
            .attr('dy', '.35em')
            .text(function(d) { return d });
    }

    function createLabels() {
        svg.append('g').append('text')
            .attr('transform', 'translate(' + ((WIDTH + MARGIN.left - MARGIN.legend)/2)
                    + ',' + (HEIGHT + MARGIN.bottom) + ')')
            .text(xInput);

        svg.append('g').append('text')
            .attr('class', 'text')
            .attr('transform', 'rotate(-90)')
            .attr('y', 20)
            .attr('x', (-HEIGHT / 2))
            .attr('dy', '0.3em')
            .text(yInput)
    }

    function createAxis() {
        main.append('g')
            .attr('transform', 'translate(0,' + HEIGHT + ')')
            .call(d3.axisBottom(x));

        main.append('g')
            .call(d3.axisLeft(y).ticks(10, 's'))
    }

    function createBars() {
        var roundCounter = 0;
        var bar = main.selectAll('.bar')
        .data(stack.keys(stacksPerBar)(dataAsArray))
        .enter().append('g')
            .attr('class', 'bar')
            .selectAll('rect')
        .data(function(d) { return d })
        .enter()
        
        var rect = bar.append('rect')
            .attr(zInput, function(d, i) { 
                if (i == 0) {
                    roundCounter++;
                }
                return roundCounter;
            })
            .attr('fill', function() { return z(($(this).attr(zInput) - 1))})
            .attr('x', function(d) { return x(d.data[xInput]); })
            .attr('y', function(d) { return y(d[1]); })
            .attr('height', function(d) { return y(d[0]) - y(d[1]); })
            .attr('width', x.bandwidth());
        createToolTip(rect);
    }

    function transformDataForStack() {
        var bars = [];

        data.sort(function(a, b) { 
            return d3.ascending(a[zInput], b[zInput]);
        });

        createArrayOfUniqueValues(bars, data, xInput);

        for (var i in bars) {
            var total = 0;
            var element = bars[i];
            var barElements = {};

            barElements[xInput] = element;
            for (var j in data) {
                if (data[j][xInput] === element) {
                    var amount = data[j][yInput];
                    var key = zInput + data[j][zInput];

                    barElements[key] = amount;
                    total += amount;
                    if (i == 0) {
                        stacksPerBar.push(key);
                    }
                }
            }
            barElements.total = total;
            dataAsArray.push(barElements);
        }
    }
}
    