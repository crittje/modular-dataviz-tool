## Basic-dataviz-tool

### UNDER CONSTRUCTION 

This tool can be used as a guide for your to create your own datavisualisation tool, or as reference for learning d3JS. 

Currently there are four visualisations available: Barchart, stacked barchart, donut, linegraph and an under construction network graph. 

The code does not contian any instructive comments, however, the function names are supposed to be descriptive and self explainatory. 

It is easy to play around and get to know the functionalities of D3JS and steps how to produce such a visualise by simply (un)commenting functions. 

Graphs contain extra features such as tooltips, highlighting of selections, hide or undhide observations and so on. 

Instead of using d3JS json(), csv() or tsv() to consume the datafunctions, the data is consumed as normal objects from a JSON file on the server. 

### Requirements
- NodeJS

## References
https://d3js.org
http://www.wkschema2018.nl

#### Donut
http://jsfiddle.net/nrabinowitz/GQDUS/

### Stacked Bar Chart
https://bl.ocks.org/DimsumPanda/689368252f55179e12185e13c5ed1fee

### Network
http://bl.ocks.org/jose187/4733747