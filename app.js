var express = require('express'),
    app = express(),
    engine = require('consolidate')
    path = require('path'),
    port = 3000;

initApplication();
setUpRoutes()

app.listen(port)
console.log('The magic happens on port ' + port)

function initApplication() {
    app.set('views', __dirname + '/views');
    app.engine('html', engine.mustache)
    app.set('view engine', 'html');
    app.use('/', express.static('public'));
    app.use('/', express.static(path.join(__dirname, 'public')));
}

function setUpRoutes() {
    app.get('/', function(req, res) {
        res.render('index')
    });

    app.get('/test', function(req, res) {
        res.render('test')
    })

    app.get('/data', function(req, res) {
        console.log('in app.js', req.query);
        
    });
}